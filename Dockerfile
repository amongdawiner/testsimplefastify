FROM node:lts

RUN apt-get update && apt-get install -y python make g++ build-essential
ENV PYTHON=/usr/bin/python

WORKDIR /app

COPY package*.json ./

RUN mkdir /.npm && chown -R 1000830000:0 "/.npm"

RUN npm install --strict-ssl=false

# RUN npm install

COPY . .

#WILL REMOVE THIS AS ENV WILL BE DEFINED IN OPENSHIFT
#RUN mv .env_example .env
#RUN npx prisma generate

EXPOSE 3001
# ENV ADDRESS=0.0.0.0 PORT=3002

CMD ["npm", "run", "dev"]
