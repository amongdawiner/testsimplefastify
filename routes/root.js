'use strict'

module.exports = async function (fastify, opts) {
  fastify.get('/', async function (request, reply) {
    return { root: true }
  })

  fastify.get('/info', async function (request, reply) {
    return {name : "NBC Bank", version : "v100", message : "Hi there, How are you"}
  })

}
