'use strict'

module.exports = async function (fastify, opts) {
  fastify.get('/', async function (request, reply) {
    return 'this is an example'
  })

  fastify.get('/health', async function (request, reply) {
    return {name : "Amani Abdallah", version : "v100", message : "Hi there, How are you"}
  })

}
